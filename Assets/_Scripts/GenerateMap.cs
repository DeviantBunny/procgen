using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ProcGen
{
    public class GenerateMap : MonoBehaviour
    {
        public List<TileMap> tiles;
        public Grid grid;
        public GameObject center;
        public Transform tileParent;
        public int mapSize;
        public Tile testTile;
        public float buildChance = 0.8f;
        public int maxTiles;
        
        [SerializeField]
        private int offset;
        [SerializeField]
        private int capacity;
        private Tile[,] tileMatrix;

        [Button("Create Grid")]
        public void CreateNewGrid()
        {
            offset = mapSize - 1;
            tileMatrix = new Tile[offset * 2, offset * 2];
            PlaceCenter();
        }

        private Tile PlaceTile(Tile tile, Vector2Int coords)
        {
            var localPos = new Vector3Int(coords.x + offset, coords.y + offset, 0);
            var worldPos = grid.CellToWorld(localPos);
            var newTile = Instantiate(tile.gameObject, worldPos, Quaternion.identity, tileParent);
            
            tileMatrix[localPos.x, localPos.y] = newTile.GetComponent<Tile>();
            return newTile.GetComponent<Tile>();
        }

        private void PlaceCenter()
        {
            var localPos = new Vector3Int(offset, offset, 0);
            var worldPos = grid.CellToWorld(localPos);
            Instantiate(center, worldPos, Quaternion.identity);
        }

        //[Button ("Print tiles")]
        public void GetTileCoordinates()
        {
            for (int i = 0; i < tileMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < tileMatrix.GetLength(1); j++)
                {
                    var item = tileMatrix[i, j];
                    if (item == null) continue;
                    Debug.Log(item + " " + "x:" + i + " " + "y:" + j);
                }
            }
        }

        /*private Tile GetRandomTile(Direction dir)
        {
            var availableTiles = tiles.FindAll(x => x.connections.Count <= capacity);
            var exclusions = availableTiles.Where(item => item.connections.Find(y => y.direction == dir) == null).ToList();

            foreach (var item in exclusions)
            {
                availableTiles.Remove(item);
            }

            return availableTiles[Random.Range(0, availableTiles.Count)];
        }*/

        private List<Vector2Int> GetEmptyAdjacentTiles(Vector2Int coords)
        {
            var freeTiles = new List<Vector2Int>();
            for (var x = - 1; x < 2; x++)
            {
                for (var y = - 1; y < 2; y++)
                {
                    if (tileMatrix[coords.x + x, coords.y + y] == null)
                    {
                        freeTiles.Add(new Vector2Int(coords.x + x, coords.y + y));
                    }
                }
            }
            return freeTiles;
        }

        [Button ("Generate Tiles")]
        public void GenerateTiles()
        {
            capacity = maxTiles;
            Array.Clear(tileMatrix, 0, tileMatrix.Length);
            foreach (var item in tileParent.transform.GetComponentsInChildren<Tile>())
            {
                DestroyImmediate(item.gameObject);
            }
            
            PlaceTile(testTile, new Vector2Int(0,0));
            var allTiles = new List<Tile>();
            var builtTiles = BuildAdjacentTiles(new Vector2Int(0 + offset,0 + offset), buildChance);

            capacity -= builtTiles.Count;
            while (capacity > 0)
            {
                allTiles.AddRange(builtTiles); 
                var randomTileIndex = Random.Range(0, builtTiles.Count);
                var activeTile = builtTiles[randomTileIndex];
                var coords = grid.WorldToCell(activeTile.transform.position);
                var pos = new Vector2Int(coords.x, coords.y);
                builtTiles = BuildAdjacentTiles(pos, buildChance);
                capacity -= builtTiles.Count;
            }
            allTiles.AddRange(builtTiles); 
            PaintWalls(allTiles);  
        }

        private List<Tile> BuildAdjacentTiles(Vector2Int coords, float chance)
        {
            var builtTiles = new List<Tile>();
            foreach (var item in GetEmptyAdjacentTiles(coords))
            {
                var pos = new Vector3Int(item.x, item.y, 0);
                if (Random.Range(0, 1) <= chance)
                {
                    var newTile = Instantiate(testTile.gameObject, grid.CellToWorld(pos), quaternion.identity, tileParent);
                    tileMatrix[item.x, item.y] = newTile.GetComponent<Tile>();
                    builtTiles.Add(newTile.GetComponent<Tile>());
                }
            }
            return builtTiles;
        }

        private bool TryGetTile(Vector2Int coords, out Tile tileOut)
        {
            var tile = tileMatrix[coords.x, coords.y];
            tileOut = tile != null ? tile : null;
            return tile != null;
        }

        private bool CheckAdjacentTile(Vector3 pos, Direction dir)
        {
            var localCoords = grid.WorldToCell(pos);
            var coords = new Vector2Int(localCoords.x, localCoords.y);
            var tile = dir switch
            {
                Direction.North => tileMatrix[coords.x, coords.y + 1],
                Direction.South => tileMatrix[coords.x, coords.y - 1],
                Direction.West => tileMatrix[coords.x - 1, coords.y],
                Direction.East => tileMatrix[coords.x + 1, coords.y],
                _ => throw new ArgumentOutOfRangeException(nameof(dir), dir, null)
            };
            return tile != null;
        }
        
        private void PaintWalls(List<Tile> allTiles)
        {
            var dir = Vector4.zero;
            foreach (var item in allTiles)
            {
                var pos = item.transform.position;
                dir.x = CheckAdjacentTile(pos, Direction.North) ? 0 : 1;
                dir.y = CheckAdjacentTile(pos, Direction.East) ? 0 : 1;
                dir.z = CheckAdjacentTile(pos, Direction.South) ? 0 : 1;
                dir.w = CheckAdjacentTile(pos, Direction.West) ? 0 : 1;

                if (dir != Vector4.zero)
                {
                    var sprite = tiles.Find(x => x.direction == dir).sprite;
                    item.GetComponent<SpriteRenderer>().sprite = sprite;
                }
            }
            
        }

        [Serializable]
        public class TileMap
        {
            public Vector4 direction;
            public Sprite sprite;
        }
    }
}