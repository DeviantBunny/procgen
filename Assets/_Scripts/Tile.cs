
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ProcGen
{
    public class Tile : MonoBehaviour
    {
        public List<Connection> connections;
    }

    [Serializable]
    public class Connection
    {
        public Transform transform;
        public Direction direction;
        public bool taken; 
    }

    public enum Direction
    {
        North,
        South,
        West,
        East
    }
}